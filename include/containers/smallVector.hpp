#pragma once

#include <vector>
#include <type_traits>
#include <stdint.h>
#include <stddef.h>

namespace bwn
{
	template<typename TypeT, size_t k_stackCount = 8, typename BaseAllocatorT = std::allocator<TypeT>>
	class small_vector_allocator
	{
		//
		// Public typedefs.
		//
	public:
		template<typename OtherTypeT>
		using BaseAllocatorRebindType = typename std::allocator_traits<BaseAllocatorT>::template rebind_alloc<OtherTypeT>;
		template<typename OtherTypeT>
		using RebindType = small_vector_allocator<OtherTypeT, k_stackCount, BaseAllocatorRebindType<OtherTypeT>>;

		using value_type = TypeT;
		// Because of a small buffer we can't allow small_vector_allocator to be movable/copyable by default.
		using propagate_on_container_move_assignment = std::false_type;
		using propagate_on_container_copy_assignment = std::false_type;
		using propagate_on_container_swap = std::false_type;
		// The small_buffer_allocators are not equal by default (they are not equal if their buffers are different)
		using is_always_equal = std::false_type;

		template <class OtherTypeT>
		struct rebind
		{
			using other = RebindType<OtherTypeT>;
		};

		//
		// Construction and destruction.
		//
	public:
		// The allocator should be always initialized with a buffer, because buffer here is an external thing.
		constexpr explicit small_vector_allocator(TypeT*const p_buffer, const BaseAllocatorT& p_baseAllocator = BaseAllocatorT()) noexcept;

		// This doesn't really makes sense because we need to initialize allocator with some buffer, and we can't use buffer of a different type.
		// template<class OtherTypeT>
		// small_vector_allocator(const RebindType<OtherTypeT>& p_other) noexcept;

		small_vector_allocator(const small_vector_allocator& p_other) noexcept;
		small_vector_allocator(small_vector_allocator&& p_other) noexcept;
		
		small_vector_allocator& operator=(const small_vector_allocator& p_other) noexcept;
		small_vector_allocator& operator=(small_vector_allocator&& p_other) noexcept;

		//
		// Public interface.
		//
	public:
		TypeT* allocate(const size_t p_count);
		void deallocate(TypeT*const p_pointer, const size_t p_count);

		// According to the C++ standard when propagate_on_container_move_assignment is set to false, the comparison operators are used
		// to check if two allocators are equal. When they are not, an element wise move is done instead of just taking over the memory.
		// For our implementation this means the comparison has to return false, if two allocators has different small buffers.
		friend constexpr bool operator==(const small_vector_allocator& p_left, const small_vector_allocator& p_right)
		{
			return p_left.m_bufferPtr == p_right.m_bufferPtr && p_left.m_baseAllocator == p_right.m_baseAllocator;
		}
		friend constexpr bool operator!=(const small_vector_allocator& p_left, const small_vector_allocator& p_right)
		{
			return !(p_left == p_right);
		}

		//
		// Private members.
		//
	private:
		// We are storing buffer externally (in a small_vector) so it would be possible to create copies of vector
		// internally in small_vector and then deallocate buffer with both of those vectors (helps with internal swaps basically).
		TypeT* m_bufferPtr;
		BaseAllocatorT m_baseAllocator;
	};

	template<typename TypeT, size_t k_stackCount = 8, typename BaseAllocatorT = std::allocator<TypeT>>
	class small_vector : public std::vector<TypeT, small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>>
	{
		//
		// Private typedefs.
		//
	private:
		using SmallAllocatorType = small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>;
		using SuperType = std::vector<TypeT, SmallAllocatorType>;

		//
		// Construction and destruction.
		//
	public:
		small_vector() noexcept;
		explicit small_vector(const BaseAllocatorT& p_baseAllocator) noexcept;
		small_vector(const small_vector& p_other);
		small_vector(const small_vector& p_other, const BaseAllocatorT& p_baseAllocator);
		small_vector(small_vector&& p_other) noexcept(std::is_nothrow_move_constructible<TypeT>::value);
		
		small_vector& operator=(const small_vector& p_other);
		small_vector& operator=(small_vector&& p_other) noexcept(std::is_nothrow_move_constructible<TypeT>::value);
		
		explicit small_vector(const size_t p_count, const BaseAllocatorT& p_baseAllocator = BaseAllocatorT());
		small_vector(const size_t p_count, const TypeT& p_value, const BaseAllocatorT& p_baseAllocator = BaseAllocatorT());
		template<class InputItT>
		small_vector(const InputItT p_begin, const InputItT p_end, const BaseAllocatorT& p_baseAllocator = BaseAllocatorT());
		small_vector(const std::initializer_list<TypeT> p_list);
		small_vector(const std::initializer_list<TypeT> p_list, const BaseAllocatorT& p_baseAllocator);

		//
		// Public interface.
		//
	public:
		void shrink_to_fit();
		bool is_small_buffer() const;

		//
		// Private methods.
		//
	private:
		SmallAllocatorType create_allocator();
		SmallAllocatorType create_allocator(const BaseAllocatorT& p_baseAllocator);

		//
		// Private members.
		//
	private:
		alignas(alignof(TypeT)) unsigned char m_buffer[k_stackCount * sizeof(TypeT)];
	};
} // namespace bwn

#include "containers/smallVector.inl"