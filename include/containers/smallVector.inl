#pragma once

#include "containers/smallVector.hpp"

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
constexpr bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::small_vector_allocator(TypeT*const p_buffer, const BaseAllocatorT& p_baseAllocator) noexcept
	: m_bufferPtr( p_buffer )
	, m_baseAllocator( p_baseAllocator )
{}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::small_vector_allocator(const small_vector_allocator& p_other) noexcept
	: m_bufferPtr( p_other.m_bufferPtr )
	, m_baseAllocator( p_other.m_baseAllocator )
{}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::small_vector_allocator(small_vector_allocator&& p_other) noexcept
	: m_bufferPtr( p_other.m_bufferPtr )
	, m_baseAllocator( std::move(p_other.m_baseAllocator) )
{}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>& bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::operator=(const small_vector_allocator& p_other) noexcept
{
	if (this != &p_other)
	{
		m_baseAllocator = p_other.m_baseAllocator;
		m_bufferPtr = p_other.m_bufferPtr;
	}
	return *this;
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>& bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::operator=(small_vector_allocator&& p_other) noexcept
{
	if (this != &p_other)
	{
		m_baseAllocator = std::move(p_other.m_baseAllocator);
		m_bufferPtr = p_other.m_bufferPtr;
	}
	return *this;
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
TypeT* bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::allocate(const size_t p_count)
{
	if (p_count <= k_stackCount)
	{
		return m_bufferPtr;
	}

	//otherwise use the default allocator
	return m_baseAllocator.allocate(p_count);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
void bwn::small_vector_allocator<TypeT, k_stackCount, BaseAllocatorT>::deallocate(TypeT*const p_pointer, const size_t p_count)
{
	if (m_bufferPtr != p_pointer)
	{
		m_baseAllocator.deallocate(p_pointer, p_count);
	}
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector() noexcept
	: SuperType( create_allocator() )
{
	SuperType::reserve(k_stackCount);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const BaseAllocatorT& p_baseAllocator) noexcept
	: SuperType( create_allocator(p_baseAllocator) )
{
	SuperType::reserve(k_stackCount);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const small_vector& p_other)
	: small_vector()
{
	SuperType::assign(p_other.begin(), p_other.end());
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const small_vector& p_other, const BaseAllocatorT& p_baseAllocator)
	: small_vector( p_baseAllocator )
{
	SuperType::assign(p_other.begin(), p_other.end());
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(small_vector&& p_other) noexcept(std::is_nothrow_move_constructible<TypeT>::value)
	: small_vector()
{
	SuperType::operator=(std::move(p_other));
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>& bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::operator=(const small_vector& p_other)
{
	if (this == &p_other)
	{
		return *this;
	}

	if (p_other.size() <= k_stackCount)
	{
		SuperType::clear();
		SuperType::shrink_to_fit();
		SuperType::reserve(k_stackCount);
		SuperType::assign(p_other.begin(), p_other.end());
	}
	else
	{
		SuperType::operator=(p_other);
	}

	return *this;
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>& bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::operator=(small_vector&& p_other) noexcept(std::is_nothrow_move_constructible<TypeT>::value)
{
	SuperType::operator=(std::move(p_other));
	return *this;
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const size_t p_count, const BaseAllocatorT& p_baseAllocator)
	: small_vector( p_baseAllocator )
{
	SuperType::resize(p_count);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const size_t p_count, const TypeT& p_value, const BaseAllocatorT& p_baseAllocator)
	: small_vector( p_baseAllocator )
{
	SuperType::assign(p_count, p_value);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
template<class InputItT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const InputItT p_begin, const InputItT p_end, const BaseAllocatorT& p_baseAllocator)
	: small_vector( p_baseAllocator )
{
	SuperType::assign(p_begin, p_end);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const std::initializer_list<TypeT> p_list)
	: small_vector()
{
	SuperType::assign(p_list.begin(), p_list.end());
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::small_vector(const std::initializer_list<TypeT> p_list, const BaseAllocatorT& p_baseAllocator)
	: small_vector( p_baseAllocator )
{
	SuperType::assign(p_list.begin(), p_list.end());
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
void bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::shrink_to_fit()
{
	const bool currentlySmallBuffer = is_small_buffer();
	const bool desiredSmallBuffer = SuperType::size() <= k_stackCount;

	if (currentlySmallBuffer && desiredSmallBuffer)
	{
		return;
	}

	if (!currentlySmallBuffer && !desiredSmallBuffer)
	{
		SuperType::shrink_to_fit();
		return;
	}

	SuperType temp(SuperType::get_allocator());
	temp.reserve(k_stackCount);
	temp.assign(std::make_move_iterator(SuperType::begin()), std::make_move_iterator(SuperType::end()));

	SuperType::clear();
	SuperType::shrink_to_fit();

	SuperType::operator=(std::move(temp));
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
bool bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::is_small_buffer() const
{
	return SuperType::data() == reinterpret_cast<const TypeT*>(m_buffer);
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
typename bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::SmallAllocatorType bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::create_allocator()
{
	return SmallAllocatorType(reinterpret_cast<TypeT*>(m_buffer));
}

template<typename TypeT, size_t k_stackCount, typename BaseAllocatorT>
typename bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::SmallAllocatorType bwn::small_vector<TypeT, k_stackCount, BaseAllocatorT>::create_allocator(const BaseAllocatorT& p_baseAllocator)
{
	return SmallAllocatorType(reinterpret_cast<TypeT*>(m_buffer), p_baseAllocator);
}
