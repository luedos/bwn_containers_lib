#define CATCH_CONFIG_MAIN 
#include "catch.hpp"

#include "containers/smallVector.hpp"

template<typename VectorT>
static bool checkVectorValue(const VectorT& p_vector, const typename VectorT::value_type& p_singleValue)
{
	for (const typename VectorT::value_type& item : p_vector)
	{
		if (item != p_singleValue)
		{
			return false;
		}
	}

	return true;
}

template<typename VectorT>
static bool checkVectorValue(const VectorT& p_vector, const std::initializer_list<typename VectorT::value_type> p_list)
{
	const size_t commonSize = std::min(p_vector.size(), p_list.size());
	for (size_t commonIndex = 0; commonIndex < commonSize; ++commonIndex)
	{
		if (p_vector[commonIndex] != *(p_list.begin() + commonIndex))
		{
			return false;
		}
	}

	return true;
}

class OverlapException : public std::exception
{
public:
	virtual const char* what() const noexcept override
	{
		return "overlap exception";
	}
};

struct OverlapTester
{
	OverlapTester()
		: value( 0 )
	{}

	OverlapTester(int _value)
		: value( _value )
	{}

	OverlapTester(const OverlapTester& _other)
		: value( _other.value )
	{
		assert(this != &_other);
	}

	OverlapTester(OverlapTester&& _other) noexcept
		: value( _other.value )
	{
		assert(this != &_other);

		_other.value = 0;
	}

	OverlapTester& operator=(const OverlapTester& _other)
	{
		assert(this != &_other);

		value = _other.value;
		return *this;
	}

	OverlapTester& operator=(OverlapTester&& _other) noexcept
	{
		assert(this != &_other);

		value = _other.value;
		_other.value = 0;
		return *this;
	}

	int value;
};

bool operator==(const OverlapTester& _left, const OverlapTester& _right)
{
	return _left.value == _right.value;
}

bool operator!=(const OverlapTester& _left, const OverlapTester& _right)
{
	return _left.value != _right.value;
}

static constexpr size_t k_bufferCount = 25;
static constexpr size_t k_smallerBufferCount = 10;
static constexpr size_t k_higherBufferCount = 35;
using SmallVectorType = bwn::small_vector<OverlapTester, k_bufferCount>;
static std::initializer_list<OverlapTester> k_smallInitializerList = { 1,2,3,4,5,6,7,8,9 };
static std::initializer_list<OverlapTester> k_bigInitializerList = { 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29 };

TEST_CASE("Simple creation")
{
	SECTION("Default creation")
	{
		SmallVectorType bufferVector;

		REQUIRE(bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == 0);
		REQUIRE(bufferVector.capacity() == 25);
	}
	
	SECTION("Mono value default creation") 
	{
		const SmallVectorType bufferVector(k_smallerBufferCount);

		REQUIRE(bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == k_smallerBufferCount);
		REQUIRE(bufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(bufferVector, 0));

		const SmallVectorType allocatorVector(k_higherBufferCount);

		REQUIRE(!allocatorVector.is_small_buffer());
		REQUIRE(allocatorVector.size() == k_higherBufferCount);
		REQUIRE(allocatorVector.capacity() == k_higherBufferCount);
		REQUIRE(checkVectorValue(allocatorVector, 0));
	}
	
	SECTION("Mono value assignment") 
	{
		const SmallVectorType bufferVector(k_smallerBufferCount, 42);

		REQUIRE(bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == k_smallerBufferCount);
		REQUIRE(bufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(bufferVector, 42));

		const SmallVectorType allocatorVector(k_higherBufferCount, 42);

		REQUIRE(!allocatorVector.is_small_buffer());
		REQUIRE(allocatorVector.size() == k_higherBufferCount);
		REQUIRE(allocatorVector.capacity() == k_higherBufferCount);
		REQUIRE(checkVectorValue(allocatorVector, 42));
	}
	
	SECTION("Iterators assignment") 
	{
		const SmallVectorType bufferVector(k_smallInitializerList.begin(), k_smallInitializerList.end());

		REQUIRE(bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == k_smallInitializerList.size());
		REQUIRE(bufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(bufferVector, k_smallInitializerList));

		const SmallVectorType allocatorVector(k_bigInitializerList.begin(), k_bigInitializerList.end());

		REQUIRE(!allocatorVector.is_small_buffer());
		REQUIRE(allocatorVector.size() == k_bigInitializerList.size());
		REQUIRE(allocatorVector.capacity() == k_bigInitializerList.size());
		REQUIRE(checkVectorValue(allocatorVector, k_bigInitializerList));
	}
	
	SECTION("Initializer list assignment") 
	{
		const SmallVectorType bufferVector(k_smallInitializerList);

		REQUIRE(bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == k_smallInitializerList.size());
		REQUIRE(bufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(bufferVector, k_smallInitializerList));

		const SmallVectorType allocatorVector(k_bigInitializerList);

		REQUIRE(!allocatorVector.is_small_buffer());
		REQUIRE(allocatorVector.size() == k_bigInitializerList.size());
		REQUIRE(allocatorVector.capacity() == k_bigInitializerList.size());
		REQUIRE(checkVectorValue(allocatorVector, k_bigInitializerList));
	}
}

TEST_CASE("Move/Copy")
{
	SECTION("Copy buffer creation")
	{
		const SmallVectorType bufferVector(k_smallInitializerList);
		const SmallVectorType copyBufferVector(bufferVector);

		REQUIRE(bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == k_smallInitializerList.size());
		REQUIRE(bufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(bufferVector, k_smallInitializerList));

		REQUIRE(copyBufferVector.is_small_buffer());
		REQUIRE(copyBufferVector.size() == k_smallInitializerList.size());
		REQUIRE(copyBufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(copyBufferVector, k_smallInitializerList));
	}

	SECTION("Copy allocator creation")
	{
		const SmallVectorType allocatorVector(k_bigInitializerList);
		const SmallVectorType copyAllocatorVector(allocatorVector);

		REQUIRE(!allocatorVector.is_small_buffer());
		REQUIRE(allocatorVector.size() == k_bigInitializerList.size());
		REQUIRE(allocatorVector.capacity() == k_bigInitializerList.size());
		REQUIRE(checkVectorValue(allocatorVector, k_bigInitializerList));

		REQUIRE(!copyAllocatorVector.is_small_buffer());
		REQUIRE(copyAllocatorVector.size() == k_bigInitializerList.size());
		REQUIRE(copyAllocatorVector.capacity() == k_bigInitializerList.size());
		REQUIRE(checkVectorValue(copyAllocatorVector, k_bigInitializerList));
	}

	SECTION("Copy buffer assignment")
	{
		const SmallVectorType bufferVector(k_smallInitializerList);
		SmallVectorType copyBufferVector(k_smallerBufferCount, 42);

		copyBufferVector = bufferVector;

		REQUIRE(bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == k_smallInitializerList.size());
		REQUIRE(bufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(bufferVector, k_smallInitializerList));

		REQUIRE(copyBufferVector.is_small_buffer());
		REQUIRE(copyBufferVector.size() == k_smallInitializerList.size());
		REQUIRE(copyBufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(copyBufferVector, k_smallInitializerList));
	}

	SECTION("Copy allocator assignment")
	{
		const SmallVectorType allocatorVector(k_bigInitializerList);
		SmallVectorType copyAllocatorVector(k_higherBufferCount, 42);

		copyAllocatorVector = allocatorVector;

		REQUIRE(!allocatorVector.is_small_buffer());
		REQUIRE(allocatorVector.size() == k_bigInitializerList.size());
		REQUIRE(allocatorVector.capacity() == k_bigInitializerList.size());
		REQUIRE(checkVectorValue(allocatorVector, k_bigInitializerList));

		REQUIRE(!copyAllocatorVector.is_small_buffer());
		REQUIRE(copyAllocatorVector.size() == k_bigInitializerList.size());
		REQUIRE(checkVectorValue(copyAllocatorVector, k_bigInitializerList));
	}

	SECTION("Move buffer creation")
	{
		SmallVectorType bufferVector(k_smallInitializerList);
		const SmallVectorType moveBufferVector(std::move(bufferVector));

		REQUIRE(bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == 0);
		REQUIRE(bufferVector.capacity() == k_bufferCount);

		REQUIRE(moveBufferVector.is_small_buffer());
		REQUIRE(moveBufferVector.size() == k_smallInitializerList.size());
		REQUIRE(moveBufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(moveBufferVector, k_smallInitializerList));
	}
	
	SECTION("Move allocator creation")
	{
		SmallVectorType allocatorVector(k_bigInitializerList);
		const SmallVectorType moveAllocatorVector(std::move(allocatorVector));

		REQUIRE(!allocatorVector.is_small_buffer());
		REQUIRE(allocatorVector.size() == 0);
		REQUIRE(allocatorVector.capacity() == k_bigInitializerList.size());

		REQUIRE(!moveAllocatorVector.is_small_buffer());
		REQUIRE(moveAllocatorVector.size() == k_bigInitializerList.size());
		REQUIRE(moveAllocatorVector.capacity() == k_bigInitializerList.size());
		REQUIRE(checkVectorValue(moveAllocatorVector, k_bigInitializerList));
	}

	SECTION("Move buffer assignment")
	{
		SmallVectorType bufferVector(k_smallInitializerList);
		SmallVectorType moveBufferVector(k_smallerBufferCount, 42);

		moveBufferVector = std::move(bufferVector);

		REQUIRE(bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == 0);
		REQUIRE(bufferVector.capacity() == k_bufferCount);

		REQUIRE(moveBufferVector.is_small_buffer());
		REQUIRE(moveBufferVector.size() == k_smallInitializerList.size());
		REQUIRE(moveBufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(moveBufferVector, k_smallInitializerList));
	}
	
	SECTION("Move allocator assignment")
	{
		SmallVectorType allocatorVector(k_bigInitializerList);
		SmallVectorType moveAllocatorVector(k_higherBufferCount, 42);

		moveAllocatorVector = std::move(allocatorVector);

		REQUIRE(!allocatorVector.is_small_buffer());
		REQUIRE(allocatorVector.size() == 0);
		REQUIRE(allocatorVector.capacity() == k_bigInitializerList.size());

		REQUIRE(!moveAllocatorVector.is_small_buffer());
		REQUIRE(moveAllocatorVector.size() == k_bigInitializerList.size());
		REQUIRE(checkVectorValue(moveAllocatorVector, k_bigInitializerList));
	}
}

TEST_CASE("Swap")
{
	SECTION("Buffer to buffer")
	{
		SmallVectorType plainCreatedVector(k_smallerBufferCount, 42);
		SmallVectorType listCreatedVector(k_smallInitializerList);

		std::swap(plainCreatedVector, listCreatedVector);

		REQUIRE(plainCreatedVector.is_small_buffer());
		REQUIRE(plainCreatedVector.size() == k_smallInitializerList.size());
		REQUIRE(plainCreatedVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(plainCreatedVector, k_smallInitializerList));

		REQUIRE(listCreatedVector.is_small_buffer());
		REQUIRE(listCreatedVector.size() == k_smallerBufferCount);
		REQUIRE(listCreatedVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(listCreatedVector, 42));
	}

	SECTION("Buffer to allocator")
	{
		SmallVectorType plainCreatedVector(k_smallerBufferCount, 42);
		SmallVectorType listCreatedVector(k_bigInitializerList);

		std::swap(plainCreatedVector, listCreatedVector);

		REQUIRE(plainCreatedVector.size() == k_bigInitializerList.size());
		REQUIRE(checkVectorValue(plainCreatedVector, k_bigInitializerList));

		REQUIRE(listCreatedVector.size() == k_smallerBufferCount);
		REQUIRE(checkVectorValue(listCreatedVector, 42));
	}

	SECTION("Allocator to allocator")
	{
		SmallVectorType plainCreatedVector(k_higherBufferCount, 42);
		SmallVectorType listCreatedVector(k_bigInitializerList);

		std::swap(plainCreatedVector, listCreatedVector);

		REQUIRE(!plainCreatedVector.is_small_buffer());
		REQUIRE(plainCreatedVector.size() == k_bigInitializerList.size());
		REQUIRE(checkVectorValue(plainCreatedVector, k_bigInitializerList));

		REQUIRE(!listCreatedVector.is_small_buffer());
		REQUIRE(listCreatedVector.size() == k_higherBufferCount);
		REQUIRE(checkVectorValue(listCreatedVector, 42));
	}
}

TEST_CASE("Shrink to fit")
{
	SECTION("Buffer to buffer")
	{
		static constexpr size_t k_halfSize = k_smallerBufferCount / 2;
		SmallVectorType bufferVector(k_smallerBufferCount, 42);

		bufferVector.resize(k_halfSize);

		REQUIRE(bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == k_halfSize);
		REQUIRE(bufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(bufferVector, 42));

		bufferVector.shrink_to_fit();

		REQUIRE(bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == k_halfSize);
		REQUIRE(bufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(bufferVector, 42));
	}

	SECTION("Allocator to buffer")
	{
		static constexpr size_t k_halfSize = k_smallerBufferCount / 2;
		SmallVectorType bufferVector(k_higherBufferCount, 42);

		bufferVector.resize(k_halfSize);

		REQUIRE(!bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == k_halfSize);
		REQUIRE(bufferVector.capacity() == k_higherBufferCount);
		REQUIRE(checkVectorValue(bufferVector, 42));

		bufferVector.shrink_to_fit();

		REQUIRE(bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == k_halfSize);
		REQUIRE(bufferVector.capacity() == k_bufferCount);
		REQUIRE(checkVectorValue(bufferVector, 42));
	}

	SECTION("Allocator to allocator")
	{
		static constexpr size_t k_doubleSize = k_higherBufferCount * 2;
		SmallVectorType bufferVector(k_doubleSize, 42);

		bufferVector.resize(k_higherBufferCount);

		REQUIRE(!bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == k_higherBufferCount);
		REQUIRE(bufferVector.capacity() == k_doubleSize);
		REQUIRE(checkVectorValue(bufferVector, 42));

		bufferVector.shrink_to_fit();

		REQUIRE(!bufferVector.is_small_buffer());
		REQUIRE(bufferVector.size() == k_higherBufferCount);
		REQUIRE(bufferVector.capacity() == k_higherBufferCount);
		REQUIRE(checkVectorValue(bufferVector, 42));
	}
}